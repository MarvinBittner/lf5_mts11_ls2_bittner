import java.util.ArrayList;
import java.util.Random;


public class Raumschiff {
	
	private Kapitaen kapitaen;
	private double energieversorgung;
	private double lebenserhaltungssysteme;
	private double schutzschilde;
	private double huelle;
	private int photonentorpedos;
	private int reparaturandroiden;
	private ArrayList<Ladung> ladungsverzeichnis;
	static private ArrayList<String> broadcastCommunicator = new ArrayList<String>();
	
	public Raumschiff() {
		this.ladungsverzeichnis = new ArrayList<Ladung>();
		this.kapitaen = new Kapitaen();
	}
	
	public Raumschiff(Kapitaen kapitaen, double energieversorgung, double lebenserhaltungssysteme, double schutzschilde,
			double huelle, int photonentorpedos, int reparaturandroiden, ArrayList<Ladung> ladungsverzeichnis, 
			ArrayList<String> braodcastCommunicator) {
		
		this.kapitaen = kapitaen;
		this.energieversorgung = energieversorgung;
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
		this.schutzschilde = schutzschilde;
		this.huelle = huelle;
		this.photonentorpedos = photonentorpedos;
		this.reparaturandroiden = reparaturandroiden;
		this.ladungsverzeichnis = ladungsverzeichnis;
		
	}

	public Kapitaen getKapitaen() {
		return kapitaen;
	}
	
	
	public void setKapitaen(Kapitaen kapitaen) {
		this.kapitaen = kapitaen;
	}
	
	
	public double getEnergieversorgung() {
		return energieversorgung;
	}
	
	
	public void setEnergieversorgung(double energieversorgung) {
		this.energieversorgung = energieversorgung;
	}
	
	
	public double getLebenserhaltungssysteme() {
		return lebenserhaltungssysteme;
	}
	
	public void setLebenserhaltungssysteme(double lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}
	
	public double getSchutzschilde() {
		return schutzschilde;
	}
	
	public void setSchutzschilde(double schutzschilde) {
		this.schutzschilde = schutzschilde;
	}
	
	public double getHuelle() {
		return huelle;
	}
	
	public void setHuelle(double huelle) {
		this.huelle = huelle;
	}
	
	public int getPhotonentorpedos() {
		return photonentorpedos;
	}
	
	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}
	
	public int getReparaturandroiden() {
		return reparaturandroiden;
	}
	
	public void setReparaturandroiden(int reparaturandroiden) {
		this.reparaturandroiden = reparaturandroiden;
	}
	
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}
	
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	
	/**
	 * Gibt die Werte aller Attribute auf der Konsole aus.
	 */
	public void zustandAusgeben() {
		
		System.out.println("Kapitaen: " + this.kapitaen.getName());
		System.out.println("Energieversorgung: " + this.energieversorgung * 100 + " %");
		System.out.println("Lebenserhaltungssysteme: " + this.lebenserhaltungssysteme * 100 + " %");
		System.out.println("Schutzschilde: " + this.schutzschilde * 100 + " %");
		System.out.println("Hülle: " + this.huelle * 100 + " %");
		System.out.println("Photonentorpedos: " + this.photonentorpedos);
		System.out.println("Reparaturandroiden: " + this.reparaturandroiden);
		
		System.out.println("Ladung:");
		for(Ladung ladung : this.ladungsverzeichnis)
			System.out.println(ladung.toString());
		
	}
	
	/**
	 * Registriert einen Treffer auf das Raumschiff.
	 */
	private void trefferRegistrieren() {
		this.schutzschilde -= 0.5;
		
		if(this.schutzschilde <= 0) {
			this.energieversorgung -= 0.5;
			this.huelle -= 0.5;
			
			if(this.huelle <= 0) {
				this.lebenserhaltungssysteme = 0;
				broadcastSenden("Lebenserhaltungssysteme zersört");
			}
		}
	}
	
	/**
	 * Sendet eine Nachricht über den BroadcastCommunicator.
	 * @param nachricht
	 */
	public void broadcastSenden(String nachricht) {
		this.broadcastCommunicator.add(nachricht);
	}
	
	/**
	 * Schießt einen Photonentorpedo ab. 
	 * @param ziel
	 */
	public void photonentorpedoAbschiessen(Raumschiff ziel) {
		
		if(this.photonentorpedos > 0) {
			this.photonentorpedos--;
			broadcastCommunicator.add("Photonentorpedo abgeschossen");
			ziel.trefferRegistrieren();		
		}else {
			broadcastCommunicator.add("-=*Click*=-");
		}		
	}
	
	
	/**
	 * Schießt die Phaserkanone ab, sofern die Energieversorgung bei min. 50% liegt. 
	 * Ansonsten wird "-=*Click*=-" ausgegeben.
	 * @Param ziel
	 */
	public void phaserkanoneAbschiessen(Raumschiff ziel) {
		
		if(this.energieversorgung >= 0.5) {
			this.energieversorgung -= 0.5;
			broadcastSenden("Phaserkanone abgeschossen");
			ziel.trefferRegistrieren();
		}else {
			broadcastSenden("-=*Click*=-");
		}
		
	}
	
	/**
	 * Gibt die Logbucheinträge aller Raumschiffe zurück.
	 * @return logbucheinträge als ArrayList<String>
	 */
	public static ArrayList<String> getLogbucheintraege(){
		return broadcastCommunicator;
	}
	
	
	/**
	 * Lädt Photonentorpedos aus der Ladung in das Raumschiff.
	 * @param anzahl
	 */
	public void torpedosLaden(int anzahl) {
		
		int count = 0;
		Ladung torpedos = null;
		while(count < this.ladungsverzeichnis.size() && torpedos == null) {
			if(this.ladungsverzeichnis.get(count).getBezeichnung() == "Photonentorpedos") {
				torpedos = this.ladungsverzeichnis.get(count);
			}
			count++;
		}
		
		if(torpedos == null || torpedos.getAnzahl() == 0) {
			System.out.println("Keine Photonentorpedos gefunden!");
			broadcastSenden("-=*Click*=-");
			return;
		}
		
		if(torpedos.getAnzahl() <= anzahl)
			anzahl = torpedos.getAnzahl();
		
		this.photonentorpedos += anzahl;
		torpedos.setAnzahl(torpedos.getAnzahl() - anzahl);
		
		System.out.println(anzahl + " Photonentorpedo(s) eingesetzt");
		
	}
	
	/**
	 * Entfernt Ladungsobjekte mit Anzahl 0 aus dem Ladungsverzeichnis.
	 */
	public void ladungAufraeumen() {
		
		for(Ladung ladung : this.ladungsverzeichnis) {
			if(ladung.getAnzahl() == 0)
				this.ladungsverzeichnis.remove(ladung);
		}
		
	}
	
	/**
	 * Setzt Reparaturandroiden zur Reparatur des Raumschiffes ein.
	 */
	public void reparieren(int androiden, boolean energieversorgung, boolean lebenserhaltungssysteme,
			boolean schutzschilde, boolean huelle) {
		
		int anzahl = 0;
		if(energieversorgung)
			anzahl++;
		if(lebenserhaltungssysteme)
			anzahl++;
		if(schutzschilde)
			anzahl++;
		if(huelle)
			anzahl++;
		
		
		if(androiden > this.reparaturandroiden)
			androiden = this.reparaturandroiden;
		
		Random rand = new Random();
		
		double wert = rand.nextInt(101) * androiden / anzahl;
		
		if(energieversorgung) {
			this.energieversorgung += wert/100;
			if(this.energieversorgung > 1.0)
				this.energieversorgung = 1.0;
		}
		if(lebenserhaltungssysteme) {
			this.lebenserhaltungssysteme += wert/100;
			if(this.lebenserhaltungssysteme > 1.0)
				this.lebenserhaltungssysteme = 1.0;
		}
		if(schutzschilde) {
			this.schutzschilde += wert;
			if(this.schutzschilde > 1.0)
				this.schutzschilde = 1.0;
		}
		if(huelle) {
			this.huelle += wert;
			if(this.huelle > 1.0)
				this.huelle = 1.0;
		}
		
	}
}
	