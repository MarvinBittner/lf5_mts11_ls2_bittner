
import java.util.ArrayList;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		
		Random rand = new Random();
		
		//Raumschiff Klingonen
		Raumschiff klingonen = new Raumschiff();
		klingonen.setPhotonentorpedos(0);
		klingonen.setEnergieversorgung(1.0);
		klingonen.setSchutzschilde(0.5);
		klingonen.setHuelle(1.0);
		klingonen.setLebenserhaltungssysteme(1.0);
		klingonen.setReparaturandroiden(2);
		
		klingonen.getLadungsverzeichnis().add(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.getLadungsverzeichnis().add(new Ladung("Batleth Klingonen Schwert", 200));
		
		//Romulaner Raumschiff
		Raumschiff romulaner = new Raumschiff();
		romulaner.setPhotonentorpedos(2);
		romulaner.setEnergieversorgung(0.5);
		romulaner.setSchutzschilde(50);
		romulaner.setHuelle(1.0);
		romulaner.setLebenserhaltungssysteme(1.0);
		romulaner.setReparaturandroiden(2);
		
		romulaner.getLadungsverzeichnis().add(new Ladung("Borg-Schrott", 5));
		romulaner.getLadungsverzeichnis().add(new Ladung("Rote Materie", 2));
		romulaner.getLadungsverzeichnis().add(new Ladung("Plasma Waffe", 50));
		
		//Vulkanier Raumschiff
		Raumschiff vulkanier = new Raumschiff();
		vulkanier.setPhotonentorpedos(3);
		vulkanier.setEnergieversorgung(rand.nextInt(101)/100);
		vulkanier.setSchutzschilde(rand.nextInt(101)/100);
		vulkanier.setHuelle(rand.nextInt()/100);
		vulkanier.setLebenserhaltungssysteme(1.0);
		vulkanier.setReparaturandroiden(5);
		
		vulkanier.getLadungsverzeichnis().add(new Ladung("Forschungssonde", 35));
		vulkanier.getLadungsverzeichnis().add(new Ladung("Photonentorpedos", 2));
		
		
		// Objekte verwenden
		
		klingonen.photonentorpedoAbschiessen(romulaner);
		romulaner.phaserkanoneAbschiessen(klingonen);
		
		vulkanier.broadcastSenden("Gewalt ist night logisch");
		
		klingonen.zustandAusgeben();
		
		vulkanier.reparieren(vulkanier.getReparaturandroiden(), true, true, true, true);
		
		
		vulkanier.torpedosLaden(2);
		
		
		klingonen.photonentorpedoAbschiessen(romulaner);
		klingonen.photonentorpedoAbschiessen(romulaner);
		
		klingonen.zustandAusgeben();
		romulaner.zustandAusgeben();
		vulkanier.zustandAusgeben();
		
		
		ArrayList<String> logbuch = Raumschiff.getLogbucheintraege();
		for(String s : logbuch)
			System.out.println(s);
		
	}

}
