import java.util.ArrayList;


public class Kapitaen {
	
	private String name;
	private int kapitaenSeit;
	private ArrayList<Raumschiff> raumschiffe;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getKapitaenSeit() {
		return kapitaenSeit;
	}
	public void setKapitaenSeit(int kapitaenSeit) {
		this.kapitaenSeit = kapitaenSeit;
	}
	public ArrayList<Raumschiff> getRaumschiffe() {
		return raumschiffe;
	}
	public void setRaumschiffe(ArrayList<Raumschiff> raumschiffe) {
		this.raumschiffe = raumschiffe;
	}

	
	
}